\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Chip description}{2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Different Column Architectures}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Setup}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Test Setup}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Dose delivery and irradiation setup}{4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Temperature and power monitoring}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Measurements}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Digital scan}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Pixel Registers scan}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Threshold scan}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Noise scan and tuning}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Timewalk scan}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusion}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}Appendices}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1}Tuned Threshold dispersion}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.2}Digital Scan - DATA clock Digital Shmoo}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.3}Digital Scan - CMD clock Digital Shmoo}{22}

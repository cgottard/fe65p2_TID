# LaTeX Makefile
FILE=fe65_irrad

all: $(FILE).pdf

.PHONY: clean

clean:
	rm -f $(FILE).aux $(FILE).blg $(FILE).out $(FILE).toc $(FILE).log
	rm -f $(FILE).pdf
$(FILE).pdf: $(FILE).tex
	pdflatex $(FILE)
	pdflatex $(FILE)
#	bibtex $(FILE)
	pdflatex $(FILE)
	pdflatex $(FILE)

\documentclass{article}
\usepackage{graphicx,amsmath,fancyhdr,multirow,array,scrextend,float}
\usepackage{subfig}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage[table,dvipsnames]{xcolor}
\usepackage{cleveref}
\usepackage[top=3.3cm, bottom=3.3cm, left=2.7cm, right=2.7cm]{geometry}
\usepackage{lineno}
\usepackage{float}
\usepackage[section]{placeins}
\usepackage{url}
\usepackage[english]{babel}
\usepackage[babel]{csquotes}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{arevmath}
\usepackage{graphicx}
\usepackage{comment}
\usepackage[backend=biber, hyperref=true,  style=numeric, backref, sorting=none]{biblatex}
\linenumbers
\addbibresource{fe65_irrad.bib}   
\graphicspath{{figures/}}
\pagestyle{fancy}
\lhead{Draft 1.0}
\rhead{\today}

\renewcommand*\familydefault{\sfdefault}
\usepackage{helvet}

\makeatletter
\def\@xfootnote[#1]{%
  \protected@xdef\@thefnmark{#1}%
  \@footnotemark\@footnotetext}
\makeatother


\makeatletter
\def\tmpp#1\@addtopreamble#2#3!{%
    \tmp#2!{#1}{#3}}
\def\tmp#1\CT@column@color\CT@row@color#2!#3#4{%
\def\@classz{#3\@addtopreamble{#1\CT@row@color\CT@column@color#2}#4}}

\expandafter\tmpp\@classz!
\makeatother


\title{\textbf{TID radiation hardness of the FE65-P2 readout chip} \\ {\small Version 1.0} }
\date{}
\author{Michael Daas, Carlo A. Gottardo, Tomasz Hemperek}
\begin{document}
\maketitle
\tableofcontents
\definecolor{Gray}{gray}{0.85}
\newcolumntype{a}{>{\columncolor{Gray}}c}
\newcolumntype{b}{>{\columncolor{blue!70!green!25}}c}
\newcolumntype{w}{>{\columncolor{white}}c}

\section{Introduction}
The FE62-P2 is a 65-nm CMOS readout chip, developed by the RD-53 collaboration, meant to be integrated in a Silicon hybrid pixel detector. 
The chip consists of a $64 \times 64$ matrix of $50 \mu m$ pitch pixels.
Its design has been conceived to meet the requirements of the ATLAS and CMS tracker upgrades for the High Luminosity phase of the Large Hadron Collider (HL-LHC).
Given the high luminosity HL-LHC is expected to deliver ($5 \times 10^{34} \text{cm}^{-2} s^{-1}$ \cite{HL}) , radiation hardness is the main concern for the chip design together with a good time resolution ($<25 ns$) and efficiency. In this note we present results about the radiation hardness to total ionizing dose (TID) effects performed by irradiating the chip with 60 keV\footnote{maximum nominal energy given by the voltage in the tube times the electron charge} x-rays up to 500 Mrad. The results will be compared to the specifications set by the RD53 collaboration \cite{RD53} which are briefly reported in \cref{tab:rd53tab}. \\ A full description of the chip can be found in \cite{fe65_doc} while an overview is given is \cref{sec:descr}.\\
In \cref{sec:setup} the experimental setup and the irradiation procedure are presented, in \cref{sec:meas} each measurement performed is first introduced and then its results are reported. Finally a conclusion is drawn in \cref{sec:conclusion}.

\begin{table}[h] \label{tab:rd53tab}
\centering
\begin{tabular}{|c|c|}
\hline
Property & RD53 Specification \\
\hline
Threshold & 600 $e^{-}$ \\
In-time threshold & 600 $e^{-}$  \\
Noise &$\sim 70 e^{-}$  \\
Power Consumption & 4 $\mu A$ per pixel at 1.2V \\
\hline
\end{tabular}
\caption{RD53 specification for HL-LHC hybrid pixel detectors \cite{RD53}.}
\end{table}

\section{Chip description}\label{sec:descr}
We hereby provide an overview of the chip following a bottom-up approach, from the pixel to the whole matrix structure.
Each pixel is equipped with the analog front-end represented in \cref{fig:schematic} whose main components are a pre-amplifier, a pre-comparator and a comparator.
These components can be regulated during operation thanks to DACs. It is possible to tune the leakage current compensation system in the pre-amplifier feedback loop, and more importantly the thresholds and the bias voltage of the  pre-comparator. In particular the global thresholds \textit{vth1, vth2} fed to the pre-comparator can be altered by a 4-bit DAC (dubbed TDAC) embedded in each pixel. A list of the chip tunable parameters is shown in \cref{tab:par} together with the description and the default value. \\ The configuration values reported in \cref{tab:par} correspond to the ones used throughout the whole test described in this note. \\ Two values have been used for \textit{preCompVbnDac} dependig on the kind of measurement and dose. \\
\begin{table}[h] 
\centering
\begin{tabular}{|l|l|l|}
\hline
PrmpVbpDac		& 	36 	& Pre-amp main bias	\\
preCompVbnDac	& 	110/50 & Pre-comparator stage (2nd stage) bias \\
vthin1Dac		& 	255 & Positive input comparator threshold	\\
vthin2Dac		& 	0 	& Negative input comparator threshold	\\
vffDac			& 	42 	& Preamp feedback current	\\         
PrmpVbnFolDac	& 	51 	& Preamp follower bias	\\ 
vbnLccDac		& 	1 	& Leakage current compensation bias	\\      
compVbnDac		& 	25 	& Comparator bias	\\      
\hline
\end{tabular}
\caption{RD53 specification for HL-LHC hybrid pixel detectors.}
\label{tab:par}
\end{table}
Two types of signal injections are possible: the analog, through the \texttt{Injin} or the digital one, through the \texttt{TestHit}. The former injection simulates an input from the sensor, while the latter bypasses all the analog circuitry and simulates a over-threshold hit which is transmitted to the digital part of the chip.\\
Groups of 4 pixels share a common logic block where four 4-bit Time-over-Threshold (ToT) 4-bit counters, seven latency counter value static memories, four 8-bit  configuration memories and the trigger ID systems are implemented. \\
When the readout is requested, the hit information is transmitted to the End-of-Column Digital logic (EOCDL) in terms of ToT units. The EOCDL is the block responsible for the readout management, data serialisation and storage of the global chip configuration. A 145-bit flip-flop memory, in fact, holds the global configuration parameters listed in \cref{tab:par}.\\
In order to work the chip needs three clock signals:
\begin{itemize}
\item the bunch crossing (CMD) clock at 40 MHz, related to the analog signal processing;
\item the data (DATA) clock at 160 MHz, fed to the digital domain,
\item the serial-parallel interface (SPI) clock used in output data transmission.
\end{itemize}
Finally, to understand the measurements hereby presented we mention that three current lines are present in the chip: Analog, Digital and Auxiliary. The nominal voltage for all three is 1.2 V with a maximum current consumption of 200 mA.\\
\begin{figure}[h]
\centering
\includegraphics[width=12cm]{schematics.png}
\caption{Pixel analog circuit schematic.}
\label{fig:schematic}
\end{figure}

\paragraph{Different Column Architectures}
The FE65-P2 is not homogeneous, eight analog design variation are implemented resulting in 8 different columns. The variations include different feedback capacitances, presence of a leakage compensation system, power down capability and finally the width (W) of the pre-comparators output transistors. The details are reported in \cref{tab:cols}, the capacitors labels $CF0$ and $CF1$ refer to the schematic. RH stands for ``radiation hard by design".

\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline
Label &  CF0/CF1 [fF] &  Power Down & Leakage Compensation &  W (load PMOS) \\
 \hline
 CPL000 &  3.36/4.99 &  no	&  no	&  0.2 \\
 CPL001 &  3.36/4.99 &  no	&  yes	&  0.2 \\
 CPL011 &  3.36/4.99 &  yes	&  yes	&  0.2 \\ 
 CPL001 RH & 3.36/4.99 &  no	&  yes	&  0.48 \\
 CPL100 &  5.15/7.76 &  no	&  no	&  0.2 \\
 CPL101 &  5.15/7.76 &  no	&  yes	&  0.2 \\
 CPL101 RH & 5.15/7.76 &  no	&  yes	&  0.48 \\
 CPL101 &  5.15/7.76 &  no	&  yes	&  0.2 \\ 
 \hline
\end{tabular}
\caption{Column design details}
\label{tab:cols}
\end{table}
\clearpage
\section{Setup}\label{sec:setup}
\subsection{Test Setup}\label{sec:descr}
The chip prototypes are mounted on custom designed PCBs and controlled by Xilinx Spartan 3 FPGAs embedded on a multi-purpose board (MIO \cite{MIOGPAC}). The PCB and MIO are interfaced by an auxiliary GPAC \cite{MIOGPAC} card as depicted in \cref{fig:pic}. A set of chip scans, each explained in the following section, has been developed in Python and runs on top of the Basil framework. An additional routine (\texttt{ScanController.py}) manages the succession of all the scans, the power supply and guarantees uniformity in the chip configuration. Before the irradiation this routine has been tested proving stability in both terms of no crashed and homogeneous results over more then 24 hours of operation. All the code used is available at \url{https://github.com/SiLab-Bonn/fe65_p2}. 
\begin{figure}[h]
\centering
\includegraphics[width=16cm]{MIO_GPAC_fe65.jpg}
\caption{Test setup for the FE65P2. On the left the MIO board with its FPGA, in the middle the GPAC card, on the right the FE65P2 PCB.  }
\label{fig:pic}
\end{figure}
\subsection{Dose delivery and irradiation setup}\label{sec:descr}
The irradiation has been performed at the KIT research institute in Karlsruhe, Germany between 31 January and 9 February 2017. The x-ray machine used is described in \cite{KIT}, no cooling aid was available. The dose has been delivered in steps with different rates, in between two steps the full chain of scans described in \cref{sec:meas} was carried out. During the irradiation periods the scans loop was kept running as well to maintain the chip powered and active with the side advantage of producing additional information. The schedule is shown in \cref{tab:doses}, where the rate reported is the one used to reach the target dose.
\begin{table}
\centering
\begin{tabular}{|c|c|}
\hline
Dose (Mrad) & Rate (krad/h) \\ 
\hline
0.1 & 200 \\
0.2 & 200 \\
0.5 & 200 \\
1.0 & 200 \\
2.0 & 499 \\
5.0 & 499 \\
10 & 499 \\
100 & 2993 \\
200 & 2993 \\
300 & 2993 \\
400 & 2993 \\
500 & 2993 \\
\hline
\end{tabular}
\caption{Dose delivery steps}
\label{tab:doses}
\end{table} 
The doses are reported in $SiO_{2}$ equivalent units according to the calibration described in \cite{KIT}.
The precise value for a given distance from the x-ray tube was provided by the software used to control the machine. The distance of the device under test (DUT) from a reference mark on the source has been measured manually with a ruler and the result fed on the program. The DUT has been kept on the same position while the current in the x-ray tube was increased. The voltage inside the x-ray tube has been kept constant at $60 kV$. \\
The spot size was $3.3 mm$ in radius, just enough to cover the F65-P2 surface which measures approximately $4 \times 3 \ mm^{2}$. The alignment has been performed with the aid of laser beamers calibrated with the aid of targets sensible to x-rays. \\
The DUT and the supporting boards were held in place by a $5 \ mm$ thick brass support that acted also as a shield for the supporting boards. Additional lead plates were laid on top to provide additional shielding and mechanical stability. \\
The temperature inside the chamber can be approximated to $25 ^{\circ}C$ even though the dry air flush inside was not sufficient to keep the temperature stable during irradiation.

\subsection{Temperature and power monitoring}\label{sec:descr}
The Printed Circuit Board (PCB) hosting the FE65 is equipped with an NTC thermistor located beneath the chip less than 1 mm aside which has been used to monitor the temperature during the irradiation and produce \cref{fig:temp}. It has been observed that, during normal operation, the measured temperature is usually one degree above the environment one. \\
As regards the power consumption, the analog, digital and auxiliary current are read during each scan. In \cref{fig:pow} the current as measured during the threshold scans of columns 1 are reported. All the three currents seem to remain constant suggesting a stable power consumption and excluding major failures of the supporting boards.
\begin{figure}[h]
\centering
\includegraphics[width=16cm]{temp.pdf}
\caption{Temperature measure by the NTC during the whole irradiation campaign.}
\label{fig:temp}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=16cm]{analog_pow1.pdf}
\caption{Current consumption as measured before the threshold scans of column 1.}
\label{fig:pow}
\end{figure}
\clearpage
\section{Measurements}\label{sec:meas}
\subsection{Digital scan}\label{sec:digi_sc}
The digital scan consists of a hit count after 100  digital injections per pixel. Under standard conditions each hit is recorded resulting in a homogeneous occupancy. This scan is repeated for different supply voltages and clock frequencies and the count of missing or additional hits is stored and considered as a gauge of the performance. At the end of the scan a so-called Shmoo plot reporting the number of missed/extra hits per configuration is produced, as in \cref{fig:ref_data}.\\
In case of complete failure of the chip the maximum number of missed hits is given by the number of repeats time the number of pixels i.e. 409600. 
The different clock frequencies probed range from 20 to 100 MHz with a step of 10 for the CMD clock and from 40 to 160\footnote{140 excluded. Cannot probe frequencies higher than 160MHz due to FPGA limitations} MHz with a step of 20 MHz for the DATA clock. For each clock frequency an independent FPGA firmware has been created, the absence of failing timing constraints had been checked and the actual clock frequencies were verified at the oscilloscope.  \\
\begin{figure}[ht]
	\centering
 	\subfloat[DATA clk. reference]
   	{\includegraphics[width=8cm]{DATA_Shmoo/ref_data_Shmoo_20170130_171425_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[CMD clk. reference]
   {\includegraphics[width=8cm]{CMD_Shmoo/ref_cmd_Shmoo_20170131_082905_.pdf}}
	\caption{Shmoo plots for the DATA clock \textbf{(a)} ad for the CMD clock \textbf{(b)} before irradiation. }
	\label{fig:ref_data}
\end{figure}

The digital scan results, for both DATA and CMD clocks, exhibited a unique trend: a slight decrease in performance up to 10 Mrad, a turning point in between 10 and 100 Mrad and then almost complete failure.
Upon inspection, the occupancy plot generated by each frequency and voltage configuration revealed a chip-wise read-out failure rather than a sparse one. The cause was then not identified, the GPAC and MIO cards were functional and adjustments to the chip configuration were not effective. As a result it was not possible to disentangle a chip malfunctioning from a setup one. \\
After 15 days of annealing at room temperature, on 24 February, a measurement with a ten-times greater delay between injections and trigger multiplicity increased from 8 to 15 revealed pristine Shmoo plots. 
The influence of the annealing cannot be properly gauged, however, between the end of the irradiation and the mentioned measurement no change had been visible in the plots. 
We conclude that a mechanism still unknown affected the readout timing but we do not consider this a chip failure. \\
A description of the measurement collected during the irradiation can be found in \cref{app:shmoo}, the CMD and DATA Shmoo plots after 500 Mrad and 15 days of annealing are reported in \cref{fig:reborn}. The effect of the irradiation appears to be barely visible.
\begin{figure}[ht]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{new_shmoo/DATA_Shmoo_20170224_161511_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{new_shmoo/CMD_Shmoo_20170227_112730_.pdf}}
	\caption{Shmoo plots for DATA \textbf{(a)} and CMD clock \textbf{(b)} after 500 Mrads, increased injection delay and trigger multiplicity}
	\label{fig:reborn}
\end{figure}


\clearpage

\subsection{Pixel Registers scan}\label{sec:proofread_sc}
As mentioned in \cref{sec:descr} each pixel has a register to store its settings. In this scan four different patterns of bits are written in the pixel registers and are read back. The number of mismatches between the written and read bits is counted. When the chip fails an array of ones is read, in such a way that in a 4 bit interval at least one bit matches, so the maximum number of errors is 12885. The same is done for the 145-bit global registers. The scan consists of a repetition of the test for different supply voltages and SPI clock frequencies (3, 4, 6, 8, 12, 16, 24, 32 MHz) such that another Shmoo plot is returned.\\
The irradiation had little effect on the registers, the number of soft errors increases slightly and, as regards the clock frequencies and supply voltages no new pattern appeared. 
\begin{figure}[h]
	\centering
	\textbf{Reference}\par 
 	\subfloat[]
   	{\includegraphics[width=8cm]{PIX_Shmoo/ref_pixels.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{PIX_Shmoo/ref_global.pdf}}
	\caption{Pixel and global registers soft errors before irradiation}\label{fig:untuned_th}
\end{figure}
\begin{figure}[h]
	\centering
	\textbf{500 Mrad}\par
 	\subfloat[]
   	{\includegraphics[width=8cm]{PIX_Shmoo/500M_pixels.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{PIX_Shmoo/500M_global.pdf}}
	\caption{Pixel and global registers soft errors at 500Mrad}\label{fig:untuned_ns}
\end{figure}
\clearpage
\subsection{Threshold scan}\label{sec:thrs_sc}
The threshold scan measures the response threshold of the pixels by providing pulses of different amplitude to the analog front-end and checking how many times the pixels respond. Each pixel of a given column is shot 100 times and the recorded hits are used to plot and fit an s-curve as in \cref{fig:fit}.
The inflection point of the s-curve is taken as the threshold while the interval lying under the slope is taken as noise. \\
\begin{figure}[h]
\centering
\includegraphics[width=8cm]{fit.png}
\label{fig:fit}
\end{figure}
The injection pulse is provided by an AGILENT A2250  function generator triggered by the scan routine. The custom pulse is shaped in such a way to have a very steep falling edge ($\approx 10 ns$) and a very long rising edge which does not cause a further positive charge injection.\\
In \cref{fig:untuned_th} and \cref{fig:untuned_ns} the averages and standard deviations of the threshold and noise are shown for each column as a function of the irradiation dose. These quantities are representative only of the pixels that fired, whose count is shown in \cref{fig:repr} (a). The missing ones are not to be considered dead: to perform this scan a pulse amplitude range and steps have to be set, however, given the different columns behaviour, the choice is not always optimal. At 10 Mrad for example the represented pixels are the ones falling in the $\sim 45 \%$ left quantile of the Gaussian threshold distribution. The range has been corrected in the next measurement.\\
All the columns follow the same trend with respect to irradiation, except the radiation hard ones.
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{untuned/thrs.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{untuned/thrs_sigma.pdf}}
	\caption{Untuned threshold and its dispersion as a function of the dose}\label{fig:untuned_th}
\end{figure}
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{untuned/noise.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{untuned/noise_sigma.pdf}}
	\caption{Untuned noise and its dispersion as a function of the dose}\label{fig:untuned_ns}
\end{figure}

\clearpage
\subsection{Noise scan and tuning}\label{sec:noise_sc}
The noise scan consists in an iterative procedure to find the pixel threshold configuration that yields the lowest global threshold. Starting from the maximum value of 255, the global threshold \texttt{vth1} is decreased by one unit in each round and the thresholds of the noisy pixels are adjusted to prevent their firing. The procedure stops when a number of four pixels remain noisy even after the tuning and thus they are disabled. 
At the end the global threshold value and the TDAC mask are returned and loaded (so the chip is now said to be tuned) by the threshold scan which is repeated as before.\\
In contrast to the untuned behaviour the RH columns (number 4 and 7) are the one responding better as the tuning manages to set a lower global threshold compared to the other columns. Moreover the lower number of un-tuned pixels is consistent with an effective tuning.\\
The large fraction of not represented pixels is discussed is appendix \cref{app:missing_tu}.\\
The discontinuity that all the columns present in the number of un-tuned pixels between 2 and 5 Mrad is due to the change of \texttt{preCompVbnDac} from 110 to 50 which had to be done to avoid time-walk scan failures.
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=7.5cm]{vth1.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=7.5cm]{unt_pix.pdf}}
	\caption{Tuned threshold and its dispersion as a function of the dose}\label{fig:tuning}
\end{figure}
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{tuned/tu_thrs.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{tuned/tu_thrs_sigma.pdf}}
	\caption{Tuned threshold and its dispersion as a function of the dose}\label{fig:untuned}
\end{figure}
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{tuned/tu_noise.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{tuned/tu_noise_sigma.pdf}}
	\caption{Tuned threshold and its dispersion as a function of the dose}\label{fig:untuned}
\end{figure}
\begin{figure}[h!]
	\centering
 	\subfloat[]
   	{\includegraphics[width=8cm]{untuned/repre_pix.pdf}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{tuned/tu_repre_pix.pdf}}
	\caption{In \textbf{(a)} fraction of pixels responding to the untuned threshold scan, in\textbf{(b)} fraction of pixels used to calculate threshold and noise.}\label{fig:repr}
\end{figure}
\clearpage
\subsection{Timewalk scan}\label{sec:tw_scan}
The timewalk scan is performed on the tuned chip and its goal is to determine which is the lowest amplitude detectable with a maximum delay of 25 ns (one bunch crossing) due to the time walk effect. A series of small pulses  with increasing amplitude are injected in a single pixel. The injection triggers the counter of a Time-to-Digital-Converter (TDC) module in the FPGA. The counter is stopped by the HITOR signal which is a common output for all the pixels.\\
The delay is thus measured and plotted against the charge injected. An exponential fit is performed on the histogram obtained and the function is intersected with an offset of 25 ns  above the fitted baseline giving the corresponding charge.\\ The linearity between the ToT measured by the TDC and the injected charge (calculated as injected pulse times capacitance) has also been measured. Two pixels have been randomly selected for each column and if both of them respond their thresholds are averaged. \\
The in-time thresholds are presented in \cref{tab:tw}. The absence of errors is deliberate, not only one or two pixels are not representative of one column, but also the variable ToT response (see \cref{fig:tw4_400}) biases the measured in-time threshold. Therefore the numbers reported do not provide a mean to rank the columns performance, but still give an indication of how well the chip performs and what is the obtainable threshold order of magnitude. \\
As mentioned in \cref{sec:noise_sc}, \texttt{preCompVbnDac} had to be switched to 50 to obtain a measurement at 5 Mrad, a value of 110 led to no output disregarding of the injection amplitude range. No time walk measure could be obtained at 500 Mrad despite the modification of the injection steps, \texttt{preCompVbnDac}, \texttt{PrmpVbpDac} or \texttt{VffDac}. \\
\begin{table}[ht]
\centering
\begin{tabular}{|c|cccccccc|}
\hline
Dose (Mrad)  & \multicolumn{8}{c|}{In-time threshold (electrons)} \\
 & col. 1 &  col. 2 & col. 3 & col. 4 & col. 5 & col. 6 & col. 7 & col. 8 \\ 
\hline 
0  &  453  &  443  &  477  &  490  &  478  &  442  &  472  &  430  \\ 
0.1  &  452  &  456  &  486  &  531  &  454  &  441  &  519  &  449  \\ 
0.2  &  447  &  475  &  545  &  426  &  487  &  455  &  454  &  524  \\ 
0.5  &  496  &  483  &  498  &  418  &  422  &  562  &  487  &  507  \\ 
1  &  478  &  495  &  511  &  463  &  476  &  524  &  510  &  611  \\ 
2  &  632  &  -  &  526  &  484  &  485  &  515  &  533  &  617  \\ 
5  &  578  &  709  &  545  &  741  &  648  &  524  &  520  &  999  \\ 
10  &  644  &  839  &  666  &  770  &  691  &  559  &  540  &  1109  \\ 
100  &  642  &  884  &  624  &  769  &  764  &  550  &  568  &  1155  \\ 
200  &  739  &  977  &  714  &  780  &  807  &  575  &  546  &  1262  \\ 
300  &  791  &  952  &  663  &  773  &  838  &  537  &  607  &  1157  \\ 
400  &  867  &  897  &  640  &  807  &  895  &  582  &  613  &  1194  \\ 
\hline
\end{tabular}
\caption{In-time thresholds }
 \label{tab:tw}
\end{table}

\begin{figure}[h!]
	\centering
	\textbf{Reference, col. 4}\par 
 	\subfloat[]
   	{\includegraphics[width=8cm]{tw/col4_ref_tw.png}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{tw/col4_ref_tot.png}}
	\caption{Timewalk scan output for col. 4 before irradiation. In Figure \textbf{(a)} is the delay as a function of the charge injection, in  \textbf{(b)} the recorded ToT as a function of the injecte pulse converted into electron count.} \label{fig:tw4_ref}
\end{figure}
\begin{figure}[h!]\label{fig:tw4_400}
	\centering
	\textbf{400 Mrad, col.4}\par 
 	\subfloat[]
   	{\includegraphics[width=8cm]{tw/col4_400M_tw.png}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{tw/col4_400M_tot.png}}
	\caption{Timewalk scan output for col. 4 at 400 Mrad delivered dose.In Figure \textbf{(a)} is the delay as a function of the charge injection, in  \textbf{(b)} the recorded ToT as a function of the injecte pulse converted into electron count.}\label{fig:untuned}
\end{figure}

\clearpage
\section{Conclusion}\label{sec:conclusion}
The FE65-P2 endured 500 Mrad of x-ray radiation at 60 keV without any failure. As regards the analog part columns 4 and 7 proved to be radiation harder than the others, as they were designed to be. Their threshold after tuning after 500 Mrad meets the RD53 requirements being around 600 electrons. No time walk information is available for the same dose but, still taking the number with caution, column 7 at 400 Mrads has a threshold around 600 electrons. In addition improvements could in be obtained by tweaking the configuration parameters whose effect are relevant and not completely understood. \\
The pixels registers proved to be radiation hard showing not even soft errors at the nominal supply voltage. \\
The digital design of the chip demonstrated its radiation hardness conditionally to the fact that two additional parameters (the trigger delay and the trigger multiplicity) had to be adjusted, enlarging the phase space of configuration parameters.
Finally, power consumption has not been evaluated in terms of power per pixel but all the currents remain constant through irradiation making the problem dose independent.\\

\appendix
\section{Appendices}
\subsection{Tuned Threshold dispersion}\label{app:missing_tu}
With reference to \cref{sec:thrs_sc} and in particular \cref{fig:unt_repr} which shows how less than a half of the pixels of column 4 fires at 10 Mrad we report in \cref{fig:unt_repr_app} the truncated threshold distribution. Here all the pixels with a threshold above the probed injection range do not fire and are stacked at the first bin. the same happens if the scanned range starts at some high value: the hit is above threshold and the pixel fires, yet it fires since the first injection and no s-curve is obtained.
\begin{figure}[h]
\centering
\includegraphics[width=8cm]{app2/10M_col4_thrsscan.png}
\caption{Untuned threshold distribution, column 4, 10 Mrad}\label{fig:unt_repr_app}
\end{figure}
\clearpage

\subsection{Digital Scan - DATA clock Digital Shmoo}\label{app:shmoo}
The Shmoo plots for the DATA clock as recorded during the irradiation campaign show a stable performance decrease up to 10 Mrad, with the first visible effects at 500 krad (see \cref{fig:irra_data1}).
A transition happens in between 10 and 100 Mrad yielding the plot of \cref{fig:irr_data2}. From 100 Mrad onwards the failures extend and no green square is visible at 500 Mrad up to 1.6 V.
To investigate what leads to the many errors in the Digital Scans let us consider the transition plot of \cref{fig:irr_data2} and its content.\\
At 1.25 V and 160 MHz the corresponding occupancy plot of \cref{fig:occ_95} (a) tells us that a pixel fired 195 hits, that makes a consistent excess of 95.
Doing the same for 1.2 V and 120 MHz we see (\cref{fig:occ_95} (b)) that the 18800 errors recorded are instead due to ordered pattern of extra hits . These kind of patterns, resembling a faulty read-out, are the responsible for the many red squares in the Shmoo plots from this ``transition point" onward, as can be seen in \cref{fig:500occ}.
These patterns are considered to arise from a premature readout or, in other terms, an asynchrony between injection and readout. As a matter of fact the increase of the delay between the injection together with the increase of the trigger multiplicity from 8 to 15 made the patterns disappear.

\begin{figure}[ht]
	\centering
 	\subfloat[Transition]
   	{\includegraphics[width=8cm]{DATA_Shmoo/500k_data_Shmoo_20170131_152441_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[100 Mrad]
   {\includegraphics[width=8cm]{DATA_Shmoo/10M_data_Shmoo_20170202_010459_.pdf}}
	\caption{Shmoo plots for the DATA clock during performance transition \textbf{(a)} and 100 Mrads in \textbf{(b)}}
	\label{fig:irr_data1}
\end{figure}

\begin{figure}[ht]
	\centering
 	\subfloat[Transition]
   	{\includegraphics[width=8cm]{DATA_Shmoo/transition_10to100M_data_Shmoo_20170202_033918_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[100 Mrad]
   {\includegraphics[width=8cm]{DATA_Shmoo/100M_data_Shmoo_20170203_083142_.pdf}}
	\caption{Shmoo plots for the DATA clock during performance transition \textbf{(a)} and 100 Mrads in \textbf{(b)}}
	\label{fig:irr_data2}
\end{figure}

\begin{figure}[h!]
	\centering
	\textbf{Reference, col. 4}\par 
 	\subfloat[]
   	{\includegraphics[width=8cm]{DATA_160_1_25V_transition.png}}
 	\hspace{0.2mm}
 	\subfloat[]
   {\includegraphics[width=8cm]{DATA_120_1_2V_transition.png}}
	\caption{Occupation plots. Figure \textbf{(a)} refers to 160 MHz and 1.25V and Figure  \textbf{(b)} to 120 MHz, 1.2V  } \label{fig:occ_95}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=8cm]{DATA_120_1_2V_500M.png}
\caption{Temperature measure by the NTC during the whole irradiation campaign.}\label{fig:500occ}
\end{figure}
\clearpage

\subsection{Digital Scan - CMD clock Digital Shmoo}
For the sake of completeness we hereby report the Shmoo plots for the CMD clock in the same manner as for the DATA ones.
\begin{figure}[ht]
	\centering
 	\subfloat[Reference]
   	{\includegraphics[width=8cm]{CMD_Shmoo/500k_cmd_Shmoo_20170131_151947_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[10 Mrad]
   {\includegraphics[width=8cm]{CMD_Shmoo/10M_cmd_Shmoo_20170202_010005_.pdf}}
	\caption{Shmoo plots for the CMD clock at 500 krad in \textbf{(a)} and 10 Mrads in \textbf{(b)}}\label{fig:irr1_cmd}
\end{figure}
\begin{figure}[ht]
	\centering
 	\subfloat[Transition]
   	{\includegraphics[width=8cm]{CMD_Shmoo/transition_10to100M_cmd_Shmoo_20170202_021740_.pdf}}
 	\hspace{0.2mm}
 	\subfloat[100 Mrad]
   {\includegraphics[width=8cm]{CMD_Shmoo/100M_cmd_Shmoo_20170203_082646_.pdf}}
	\caption{Shmoo plots for the CMD clock during performance transition \textbf{(a)} and 100 Mrads in \textbf{(b)}}\label{fig:irr2_cmd}
\end{figure}
\clearpage
\nocite{*}
\printbibliography
\end{document}